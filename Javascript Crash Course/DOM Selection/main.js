console.log(document.getElementById('my-form'));
console.log(document.querySelector('.container'));

// multiple selectior atau ambil lebih dari 1 selektor DOM
console.log(document.querySelectorAll('.item'));
console.log(document.getElementsByTagName('li'));
console.log(document.getElementsByClassName('item'));

const items = document.querySelectorAll('.item');
items.forEach((item) => console.log(item));
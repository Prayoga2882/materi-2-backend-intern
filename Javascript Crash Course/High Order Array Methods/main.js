// forEach, map, filter
todos.forEach(function(todo) {
    console. log(todo.text);
    });

todos.map(function(todo) {
        console. log(todo.text);
    });

const todoText = todos.map(function(todo) {
    return todo.text;
});

console.log(todoText)

const todoCompleted = todos.filter(function(todo) {
    return todo.isCompleted === true;
}).map(function(todo) { 
    return todo.text;
})
    console. log(todoCompleted);
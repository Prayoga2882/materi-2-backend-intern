// let, const, var

// this is penganut level scope
const age = 30;

age = 21;

// assigning with let

let age1;

age1 = 20;

console.log(age1)